#!/usr/bin/env bash
set -e
[[ ${AUTO_UPDATE} == 1 ]] && ./update.sh
./teamspeak3-server_linux_amd64/ts3server_minimal_runscript.sh