#!/usr/bin/env bash
INSTALL_ARCHIVE=teamspeak3-server.tar.bz2
if [[ ! -f ${INSTALL_ARCHIVE} ]]
then
    wget -O teamspeak3-server.tar.bz2 https://files.teamspeak-services.com/releases/server/3.6.1/teamspeak3-server_linux_amd64-3.6.1.tar.bz2
    tar xf teamspeak3-server.tar.bz2
fi

